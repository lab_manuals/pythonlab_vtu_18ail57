#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
01bCheckPrime.py

Write a python program to check whether the given number is prime or not?
Created on Tue Nov 22 11:21:37 2022

@author: Prabodh C P
"""

import math
val = int(input("Enter a number to check for primality : "))
isPrime = True
for i in range(2,math.floor(math.sqrt(val))):
    if val % i == 0:
        isPrime = False
        
if isPrime:
    print("Number is Prime")
else:
    print("Number is not Prime")
    