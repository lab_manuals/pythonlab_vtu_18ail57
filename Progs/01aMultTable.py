# -*- coding: utf-8 -*-
"""
01aMultTable.py

Write a python program to print the multiplication table for the given number

Created on Tue Nov 22 11:21:37 2022

@author: Prabodh C P
"""


val = int(input("Enter a number : "))
for i in range(1,11):
	print(val, '*', "%2s"% (i), '=', "%3s"% (val*i) )