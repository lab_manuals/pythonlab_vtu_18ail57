#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
02aListOps.py

Write a python program to implement List operations (Nested List,Length,Concatenation, Membership, Iteration, Indexing and Slicing)

Created on Tue Nov 22 11:30:13 2022

@author: putta
"""

myList1 = []
myList2 = []


num = int(input("Enter the number of elements in your list 1 : "))

for i in range(num):
    val = int(input("Enter the element : "))
    myList1.append(val)
    
print('The length of list1 is', len(myList1))

print('List 1', myList1)

num = int(input("Enter the number of elements in your list 2 : "))

for i in range(num):
    val = int(input("Enter the element : "))
    myList2.append(val)
    
print('The length of list2 is', len(myList2))

print('List 2', myList2)

#List concatenation
print("List concatenation")

myList3 = myList1 + myList2

print('List 3', myList3)

#Nested List
print("Nested List")
myList4 = [myList1, myList2]

print('List 4', myList4)

# print("Checking Membership")
# print('List 1', myList1)
# elem = int(input("Enter the element to check for membership in List 1 : "))

# if elem in myList1:
#     print(elem, 'is a member')
# else:
#     print(elem, 'is not a member')
    
# print('List1 Iteration')

# cnt = 1
# for i in myList1:
#     print('Element' , i)
    

# print('Indexing')
# idx = int(input("Enter the idx : ")) #valid negative indexes can also be given
# if idx < -len(myList1) or idx >= len(myList1):
#     print('Invalid Index')
# else:
#     print('Element at index', idx, 'is', myList1[idx])

    
# print('List2 Iteration with indexing')

# for i in range(len(myList2)):
#     print('Element at index', i, 'is', myList2[i] )
    

# print('List 1', myList1)

# print('Slicing on List1')
# print('The length of list1 is', len(myList1))

# m,n = eval(input("Enter Slice indexes"))

# print(myList1[m:n])