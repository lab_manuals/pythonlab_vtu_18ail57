#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

05bDictOPns.py

Write a program to illustrate Dictionary operations([], in,traversal)and methods: 
keys(),values(),items()
Created on Tue Nov 29 00:16:47 2022

@author: putta
"""

#Testing Whether a Key Is in a Dictionary
fruit_prices = {'apples': 75, 'pears': 225,'peaches': 174,'bananas': 49}


print(fruit_prices)
#Illustrating []
key = input("Enter the fruit name")
if key in fruit_prices.keys():
    rate = fruit_prices[key]
    print('The rate of', key, 'is',rate)
else:
    print("Invalid key")
#Illustrating in
print('apples' in fruit_prices)
print('oranges' in fruit_prices)

#Traversal
print('Illustrating Traversal on Dictionaries')
for key, value in fruit_prices.items():
    print(key,value)
    
#items() method    
print('Illustrating items() method on Dictionaries')

for item in fruit_prices.items():
    print(item[0], item[1])
    
#values() method    
print('Illustrating values() method on Dictionaries')

for value in fruit_prices.values():
    print(value)

#keys() method
print('Illustrating keys() method on Dictionaries')

for key in fruit_prices.keys():
    print(key)
    