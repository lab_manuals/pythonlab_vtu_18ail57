#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""

01cFactorial.py

Write a python program to find factorial of the given number?

Created on Tue Nov 22 11:24:36 2022

@author: putta
"""

val = int(input("Enter a non negative number whose Factorial is to be calculated : "))

prod = 1
for i in range(1, val+1):
    prod = prod * i
    
print('Factorial of', val, 'is', prod)