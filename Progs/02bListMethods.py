#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
02bListMethods.py

Write a python program to implement List methods (Add, Append, Extend & Delete)

Created on Fri Nov 25 02:47:16 2022

@author: putta
"""

myList1 = []
myList2 = []


num = int(input("Enter the number of elements in your list 1 : "))

for i in range(num):
    val = int(input("Enter the element : "))
    myList1.append(val)
    
print('The length of list1 is', len(myList1))

print('List 1', myList1)

elem = int(input('Enter element to be added to the list : '))
pos = int(input('Enter position where the element has to be added: '))

myList1.insert(pos, elem)

print('List 1 after adding element', myList1)

num = int(input("Enter the number of elements in your list 2 : "))

for i in range(num):
    val = int(input("Enter the element : "))
    myList2.append(val)
    
print('The length of list2 is', len(myList2))

print('List 2', myList2)

myList1.extend(myList2)
print('List 1 after extending', myList1)

elem = int(input('Enter element to be deleted : '))
myList1.remove(elem)

print('List 1 after deleting element', myList1)
