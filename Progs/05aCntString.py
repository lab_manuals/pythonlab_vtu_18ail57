#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
05aCntString.py

Write a python program to implement a function that counts the number of times a string(s1) occurs in another string(s2)
Created on Mon Nov 28 17:43:34 2022

@author: putta
"""

def cntString(s1, s2):
    cnt = 0
    for i in range(len(s2)):
        chkstr = s2[i:]
        if chkstr.startswith(s1):
            cnt += 1
    print(s1, 'appears', cnt, 'times in', s2)
    
cntString('abc', 'asabcjdkjdabcahj')